# Social Coding Community Guidance

This repository holds relevant information related to our [Social Coding Community Participation Guidelines](https://codeberg.org/SocialCoding/community/src/branch/main/index.md) and other informational resources that involve the Social Coding Movement, and in particular our direct community and ecosystem.

## How to reference our CoC?

The Social Coding Community Participation Guidelines can be referenced by adding a `CODE_OF_CONDUCT.md` file to your repository with the following text:

```md
# Community Participation Guidelines

This repository is governed by Social Coding Movement's code of conduct and etiquette guidelines. By your interaction you accept to abide by its rules.

For more details, please read the [Social Coding Community Participation Guidelines](https://participate.coding.social). 

## How to Report

For more information on how to report violations of the Community Participation Guidelines, please read the '[Reporting](https://participate.coding.social/#reporting)' section.
```


## Requesting an Update

To request an update, or change to Social Coding Community Participation Guidelines, open an issue in this repository if the changes are minor (e.g. typo's). For larger updates please refer to our [discussion forum](https://discuss.coding.social) and create your topic in the _Community_ category.
